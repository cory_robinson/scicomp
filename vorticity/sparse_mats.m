function [A, B, C] = sparse_mats(m,dx,dy)
% Cory Robinson
%
% Modified from Homework 3 to work with Homework 4
% Modified on 11/15/2013
%
% CREATION OF MATRICES A,B,C BY THE CENTRAL DIFFERENCE METHOD FOR SOLVING
% THE STREAM-VORTICITY EQUATIONS.
%



n = m*m;

e0 = zeros(n,1);    % vector of zeros
e1 = ones(n,1);     % vector of ones

e2 = e1;    % copy the one vector
e4 = e0;    % copy the zero vector

%-----------------------------------------------------------
% Creating matrix A
for j = 1:m
    e2(m*j) = 0;    % overwrite every m^th value with zero
    e4(m*j) = 1;    % overwrite every m^th value with one
end

e3(2:n,1) = e2(1:n-1,1); e3(1,1) = e2(n,1);  % shift to correct
e5(2:n,1) = e4(1:n-1,1); e5(1,1) = e4(n,1);  % positions

% place diagonal elements
A = spdiags([e1 e1 e5 e2 -4*e1 e3 e4 e1 e1],...
    [-(n-m) -m -m+1 -1 0 1 m-1 m (n-m)],...
    n,n);
A(1,1) = 2;         % changed so that A will be nonsingular
A = 1/(dx^2) * A;

%figure(1); spy(A); title('spy(A)');
%A1 = sparse(A);





%----------------------------------------------------------
% Creating matrix B:
B = spdiags([e1 -e1 e1 -e1],...
    [-(n-m) -m m (n-m)],...
    n,n);
B = 1/(2*dx) * B;
%figure(2); spy(B);
%A2 = sparse(B);





%----------------------------------------------------------
% Creating matrix C:
C = spdiags([e5 -e2 e3 -e4],...
    [-m+1 -1 1 m-1],...
    n,n);
% corners are -1(upper right) and 1(lower left)
C = 1/(2*dy)*C;
%figure(3); spy(C);
%A3 = sparse(C);