function omega0 = gauss_bump( x,y )

% This function is an elliptic Gaussian bump, strecthed in the y-direction.
% This function serves as an initial condition for the vorticity function.

omega0 = exp(-x.^2 - (y.^2 / 20)).';

end

