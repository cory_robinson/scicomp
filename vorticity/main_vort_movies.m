clear all; close all;

% main script for solving Homework 4 problem on the Vorticity-Stream
% Function.
% 
% DEPENDENDENT ON THE FOLLOWING FUNCTION FILES:
%   1.  sparse_mats.m  - contains matrices A,B,C from the central difference
%                       scheme
%   2.  vort_FFT.m - solves the vorticity equation with the FFT method
%
% Created by Cory Robinson on 11/19/2013
%


%-------------------------------------------------------------------------
% SETTING PARAMETERS, INTERVALS, SPANS, ETC.
m = 64;      % n = m*m = 4096 FOR n = 64
n = m*m;
L = 20;
v = 0.001;      % diffusion parameter
tspan = [0:0.5:75];
x2 = linspace(-L/2,L/2,m+1);
x = x2(1:m);
y = x;
[X,Y] = meshgrid(x,y);
interval = L/2 - (-L/2);
dx = interval/m;
dy = dx;


%-------------------------------------------------------------------------
% THESE K PARAMETERS WILL BE USED ONLY IN THE FFT METHOD
kx=(2*pi/L)*[0:(m/2-1) (-m/2):-1];
kx(1)=10^-6;
ky=kx;
[KX,KY] = meshgrid(kx,ky);
K = -(KX.^2 + KY.^2);    % size 64x64
% Note that -K rotates counterclockwise, while +K rotates clockwise



%-------------------------------------------------------------------------
% GETTING MATRICES A, B, AND C BY A FUNCTION CALL
% NOTE THAT A(1,1) = 2 IS SET IN sparse_mats.m
[A,B,C] = sparse_mats(m,dx,dy);



%-------------------------------------------------------------------------
% SETTING GAUSSIAN BUMP FOR INITIAL CONDITION
%omega0 = exp(-X.^2 - (Y.^2 / 20));  % 64x64
%omega0 = exp(-0.25*(X).^2-2*Y.^2);
%omega0 = -exp(-0.25*(X-2).^2-2*Y.^2)+exp(-0.25*(X+2).^2-2*Y.^2);
%omega0 = -exp(-0.25*(X-2).^2-2*(Y+6).^2)...
%        +exp(-0.25*(X+2).^2-2*(Y+6).^2)...
%        +exp(-0.25*(X-2).^2-2*(Y-6).^2)...
%        -exp(-0.25*(X+2).^2-2*(Y-6).^2);






%-------------------------------------------------------------------------
% EVOLVE THE VORTICITY IN TIME BY RUNNING THE ODE SOLVERS:
% As a first guess I would think that A\b method will be the faster method
% between LU and A\b.  But I was wrong, the reason is that LU is only being
% factorized once and the same LU is being used to solve multiple psi; on
% the other hand, the A\b method has to run thru the whole gaussian
% elimination method for each time step which results in more computations
% to make.  If LU were being factored in the vort_LU function then
% computation would take a lot longer.



% FAVORITE METHOD IS FFT METHOD
%-------------------------------------------------------------------------

% MOVIE #1  ---   2 OPPOSITE CHARGED VORTICES NEXT TO EACH OTHER
 % gaussian bump initial condition
omega0 = -exp(-0.10*(X-2).^2-2*Y.^2)+exp(-0.10*(X+2).^2-2*Y.^2); 
omega0 = reshape(omega0,n,1);  % 4096x1  reshape gaussian bump
tic;
[t,omega_sol] = ode45(@(t,omega) vort_FFT(t,omega,v,A,B,C,K),tspan,omega0);
toc;
% Elapsed time is 0.070357 seconds, on my Linux Machine
% Elapsed time is 0.102923 seconds, on my MacBook Air

% PLOTTING THE RESULTS AS A FUNCTION OF TIME AND SPACE:
%waterfall(X,t,omega1_sol)

writerObj = VideoWriter('opp_charg_2vort.avi')  % name of movie
writerObj.FrameRate = 24;  % frames per second  
writerObj.Quality = 100;
% (lasts about 6 sec for tspan[0:60]
open(writerObj);
% PLOTTING MOVIE FRAMES
figure(1);
for j=1:length(t)
    
    curl_omega = reshape(omega_sol(j,:),m,m);
    
    %subplot(2,2,1);
    min_x = min(min(X));
    min_y = min(min(Y));
    max_x = min(min(X));
    max_y = min(min(Y));
    
    planeimg = curl_omega;
    pcolor(X,Y,curl_omega);
    colormap jet;
    material metal;
    shading interp;
    lighting phong;
    axis square;
    xlabel('x'); ylabel('y'); zlabel({'z',...
        '(top view)'});
    title({'\bf 2D Plot of 2 Oppisitely Charged',...
        'Gaussian Vortices Evolving in Time'});
    
    
    M(j) = getframe(gcf);
    
    pause(0.00001);
end
writeVideo(writerObj,M);
close(writerObj);






%{
%-------------------------------------------------------------------------
% MOVIE #2   ---   2 SAME CHARGED VORTICES NEXT TO EACH OTHER
% gaussian bump initial condition
omega0 = exp(-0.10*(X).^2-2*(Y+2).^2) + exp(-0.10*(X).^2-2*(Y-2).^2);  
omega0 = reshape(omega0,n,1);  % 4096x1  reshape gaussian bump
tic;
[t,omega_sol] = ode45(@(t,omega) vort_FFT(t,omega,v,A,B,C,K),tspan,omega0);
toc;
% Elapsed time is 0.070357 seconds, on my Linux Machine
% Elapsed time is 0.102923 seconds, on my MacBook Air

% PLOTTING THE RESULTS AS A FUNCTION OF TIME AND SPACE:
%waterfall(X,t,omega1_sol)

writerObj = VideoWriter('same_charg_2vort.avi')  % name of movie
writerObj.FrameRate = 24;  % frames per second 
% (lasts about 6 sec for tspan[0:60]
open(writerObj);
% PLOTTING MOVIE FRAMES
figure(2);
for j=1:length(t)
    
    curl_omega = reshape(omega_sol(j,:),m,m);
    
    subplot(2,2,1);
    surf(X,Y,curl_omega);
    colormap hsv;
    shading interp;
    lighting phong;
    axis square;
    xlabel('x'); ylabel('y'); zlabel({'z',...
        '(top view)'});
    %title('3D Surface Plot of Vortices');
    %view(15,-10);
    
    neg_curl_omega = -1*curl_omega;
    subplot(2,2,3);
    surf(X,Y,neg_curl_omega);
    colormap jet;
    shading interp;
    lighting phong;
    axis square;
    xlabel('x'); ylabel('y'); zlabel({'z',...
        '(bottom view)'});
    title('3D Surface Plot of Vortices');
  
    subplot(1,2,2);
    pcolor(X,Y,curl_omega);
    colormap hsv;
    lighting phong;
    shading interp;
    axis square;
    xlabel('x'); ylabel('y');
    title({'2D Plot of Vortices'})
    
    
    ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],...
        'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');

    text(0.5, 1,{'\bf Time Evolution Plot of Two Same Charged',...
        'Gaussian Vortices Next to Each Other'},'HorizontalAlignment',...
        'center','VerticalAlignment', 'top');
    
    
    M(j) = getframe(gcf);
    
    pause(0.00001);
end
writeVideo(writerObj,M);
close(writerObj);







%-------------------------------------------------------------------------
% MOVIE #3   ---   2 PAIRS OF OPPOSITELY CHARGED VORTICES WHICH COLLIDE
% WITH EACH OTHER
% gaussian bump initial condition
omega0 = -exp(-0.25*(X-2).^2-2*(Y+6).^2)...
        +exp(-0.25*(X+2).^2-2*(Y+6).^2)...
        +exp(-0.25*(X-2).^2-2*(Y-6).^2)...
        -exp(-0.25*(X+2).^2-2*(Y-6).^2);  
omega0 = reshape(omega0,n,1);  % 4096x1  reshape gaussian bump
tic;
[t,omega_sol] = ode45(@(t,omega) vort_FFT(t,omega,v,A,B,C,K),tspan,omega0);
toc;
% Elapsed time is 0.070357 seconds, on my Linux Machine
% Elapsed time is 0.102923 seconds, on my MacBook Air

% PLOTTING THE RESULTS AS A FUNCTION OF TIME AND SPACE:
%waterfall(X,t,omega1_sol)

writerObj = VideoWriter('pairs_colliding_vort.avi')  % name of movie
writerObj.FrameRate = 24;  % frames per second
writerObj.Quality = 100;
% (lasts about 6 sec for tspan[0:60]
open(writerObj);
% PLOTTING MOVIE FRAMES
figure(3);
for j=1:length(t)
    
    curl_omega = reshape(omega_sol(j,:),m,m);
    
    subplot(1,2,1);
    surfc(X,Y,curl_omega);
    colormap lines;
    shading interp;
    lighting phong;
    axis square;
    xlabel('x'); ylabel('y'); zlabel({'z',...
        '(top view)'});
    title({'3D Plot of Vortices'});
    view(15,25);
    
    subplot(1,2,2);
    pcolor(X,Y,curl_omega);
    colormap hsv;
    lighting phong;
    shading interp;
    axis square;
    xlabel('x'); ylabel('y');
    title({'2D Plot of Vortices'})
    
    
    ha = axes('Position',[0 0 1 1],'Xlim',[0 1],'Ylim',[0 1],...
        'Box','off','Visible','off','Units','normalized', 'clipping' , 'off');

    text(0.5, 1,{'\bf Time Evolution Plots of Two Pairs of Oppositely Charged',...
        'Vortices Colliding With Each Other'},'HorizontalAlignment',...
        'center','VerticalAlignment', 'top');
    
    
    M(j) = getframe(gcf);
    
    pause(0.00001);
end
writeVideo(writerObj,M);
close(writerObj);







%-------------------------------------------------------------------------
% MOVIE #4   ---   MANY RANDOM VORTICES

% SETTING PARAMETERS, INTERVALS, SPANS, ETC. ONLY FOR MOVIE 4
m = 64;      % n = m*m = 4096 FOR n = 64
n = m*m;
L = 30;
v = 0.001;      % diffusion parameter
tspan = [0:0.5:75];
x2 = linspace(-L/2,L/2,m+1);
x = x2(1:m);
y = x;
[X,Y] = meshgrid(x,y);
interval = L/2 - (-L/2);
dx = interval/m;
dy = dx;

% THESE K PARAMETERS WILL BE USED ONLY IN THE FFT METHOD
kx=(2*pi/L)*[0:(m/2-1) (-m/2):-1];
kx(1)=10^-6;
ky=kx;
[KX,KY] = meshgrid(kx,ky);
K = -(KX.^2 + KY.^2);    % size 64x64
% Note that -K rotates counterclockwise, while +K rotates clockwise

% gaussian bump initial condition    
omega0 = -exp(-0.67*(X-1).^2-1.1*(Y+3).^2)...
        +exp(-2*(X+1.6).^2-0.23*(Y-1).^2)...
        +exp(-1.34*(X+0.32).^2-0.96*(Y-2.1).^2)...
        -exp(-2.67*(X+0.92).^2-0.2*(Y+7.8).^2)...
        -exp(-3.6*(X-8.4).^2-1.34*(Y-5.3).^2)...
        +exp(-7.3*(X-6.3).^2-3.46*(Y+9.1).^2)...
        +exp(-0.876*(X-4.5).^2-1.345*(Y-7.3).^2)...
        -exp(-0.43*(X+8.23).^2-0.75*(Y+7.12).^2)...
        -exp(-3.26*(X+9.23).^2-6.45*(Y+5.89).^2)...
        +exp(-7.65*(X+3).^2-7.396*(Y+8.21).^2)...
        +exp(-10.836*(X).^2-1.246*(Y-5).^2)...
        +exp(-1.836*(X+10).^2-0.8246*(Y-7.5).^2)...
        -exp(-0.978*(X+8.23).^2-1.023*(Y-8.89).^2)...
        -exp(-0.593*(X-9.6).^2-14.72*(Y-3.67).^2);
    
    
omega0 = reshape(omega0,n,1);  % 4096x1  reshape gaussian bump
tic;
[t,omega_sol] = ode45(@(t,omega) vort_FFT(t,omega,v,A,B,C,K),tspan,omega0);
toc;
% Elapsed time is 0.070357 seconds, on my Linux Machine
% Elapsed time is 0.102923 seconds, on my MacBook Air

% PLOTTING THE RESULTS AS A FUNCTION OF TIME AND SPACE:
%waterfall(X,t,omega1_sol)

writerObj = VideoWriter('many_random_vort.avi');  % name of movie
writerObj.FrameRate = 24;  % frames per second 
% (lasts about 6 sec for tspan[0:60]
open(writerObj);
% PLOTTING MOVIE FRAMES
figure(4);
for j=1:length(t)
    
    curl_omega = reshape(omega_sol(j,:),m,m);
    
        
    pcolor(X,Y,curl_omega);
    colormap hot;
    material metal;
    lighting phong;
    shading interp;
    axis square;
    xlabel('x'); ylabel('y');
    title({'\bf Time Evolution Plot of Many Random Gaussian',...
        'Vortices That Collide With Each Other'})
    
    
    M(j) = getframe(gcf);
    
    pause(0.00001);
end
writeVideo(writerObj,M);
close(writerObj);
%}
%}