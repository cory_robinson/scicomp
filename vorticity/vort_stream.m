function [rhs1,rhs2] = vort_stream( t,omega,v,A,B,C,L,U )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
psi1 = A\omega;         % solve with A\b
psi2 = U\(L\omega);     % solve with LU
%sp1 = size(psi1)
%sp2 = size(psi2)
% Modify to solve using FFT at a later time
%(B*psi1).*(C*omega);    % can delete later, used for debugging
rhs1 = -(B*psi1) .* (C*omega) + (C*psi1) .* (B*omega) + v*A*omega;
rhs2 = -(B*psi2) .* (C*omega) + (C*psi2) .* (B*omega) + v*A*omega;


end

