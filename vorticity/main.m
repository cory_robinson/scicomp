clear all; close all;

% main script for solving Homework 4 problem on the Vorticity-Stream
% Function.
% 
% DEPENDENDENT ON THE FOLLOWING FUNCTION FILES:
%   1.  sparse_mats.m  - contains matrices A,B,C from the central difference
%                       scheme
%   2.  vort_Ab.m  - solves the vorticity equation with A\b method
%   3.  vort_LU.m  - solves the vorticity equation with the LU method
%   4.  vort_FFT.m - solves the vorticity equation with the FFT method
% TODO:
%   5.  vort_bstab.m - solves the vorticity equation with bicgstab method
%   6.  vort_gmres.m - solves the vorticity equation with gmres method
%
% Created by Cory Robinson on 11/15/2013
%


%-------------------------------------------------------------------------
% SETTING PARAMETERS, INTERVALS, SPANS, ETC.
m = 64;      % n = m*m = 4096 FOR n = 64
n = m*m;
L = 20;
v = 0.001;      % diffusion parameter
tspan = [0:0.5:50];
x2 = linspace(-L/2,L/2,m+1);
x = x2(1:m);
y = x;
[X,Y] = meshgrid(x,y);
interval = L/2 - (-L/2);
dx = interval/m;
dy = dx;


%-------------------------------------------------------------------------
% THESE K PARAMETERS WILL BE USED ONLY IN THE FFT METHOD
kx=(2*pi/L)*[0:(m/2-1) (-m/2):-1];
kx(1)=10^-6;
ky=kx;
[KX,KY] = meshgrid(kx,ky);
K = -(KX.^2 + KY.^2);    % size 64x64
% Note that -K rotates counterclockwise, while +K rotates clockwise



%-------------------------------------------------------------------------
% GETTING MATRICES A, B, AND C BY A FUNCTION CALL
% NOTE THAT A(1,1) = 2 IS SET IN sparse_mats.m
[A,B,C] = sparse_mats(m,dx,dy);

% LU FACTORIZATION OF A FOR USE IN THE LU SOLVER METHOD
[L,U] = lu(A);


%-------------------------------------------------------------------------
% SETTING GAUSSIAN BUMP FOR INITIAL CONDITION
omega0 = exp(-X.^2 - (Y.^2 / 20));  % 64x64
%omega0 = exp(-0.25*(X).^2-2*Y.^2);
%omega0 = -exp(-4*(X-2).^2-4*(Y.^2 / 10))-5*exp(-(X+2).^2-0.25*Y.^2);
%omega0 = -exp(-0.25*(X-2).^2-2*(Y+6).^2)...
%        +exp(-0.25*(X+2).^2-2*(Y+6).^2)...
%        +exp(-0.25*(X-2).^2-2*(Y-6).^2)...
%        -exp(-0.25*(X+2).^2-2*(Y-6).^2);

omega0 = reshape(omega0,n,1);  % 4096x1




%-------------------------------------------------------------------------
% EVOLVE THE VORTICITY IN TIME BY RUNNING THE ODE SOLVERS:
% As a first guess I would think that A\b method will be the faster method
% between LU and A\b.  But I was wrong, the reason is that LU is only being
% factorized once and the same LU is being used to solve multiple psi; on
% the other hand, the A\b method has to run thru the whole gaussian
% elimination method for each time step which results in more computations
% to make.  If LU were being factored in the vort_LU function then
% computation would take a lot longer.

%options = odeset('Stats' , 'on' ); % Print diagnostics
%{
% 1ST UP IS THE A/b METHOD:
tic;
[t,omega1_sol] = ode45(@(t,omega) vort_Ab(t,omega,v,A,B,C),tspan,omega0);
toc;
% Elapsed time is 2.450688 seconds, on my Linux Machine
% Elapsed time is 2.278158 seconds, on my MacBook Air



% 2ND IS THE LU METHOD:
tic;
[t,omega2_sol] = ode45(@(t,omega) vort_LU(t,omega,v,A,B,C,L,U),tspan,omega0);
toc;
% Elapsed time is 0.261881 seconds, on my Linux Machine
% Elapsed time is 0.493089 seconds, on my MacBook Air
%}


% 3RD IS THE FFT METHOD
tic;
[t,omega3_sol] = ode45(@(t,omega) vort_FFT(t,omega,v,A,B,C,K),tspan,omega0);
toc;
% Elapsed time is 0.070357 seconds, on my Linux Machine
% Elapsed time is 0.102923 seconds, on my MacBook Air


%{
% 4th IS THE BICGSTAB METHOD
tic;
[t,omega4_sol] = ode45(@(t,omega) vort_bstab(t,omega,v,A,B,C),tspan,omega0);
toc;
% Elapsed time is 2.682957 seconds, on my Linux Machine
% Elapsed time is  seconds, on my MacBook Air
%}

%{
% 5th IS THE GMRES METHOD
tic;
[t,omega5_sol] = ode45(@(t,omega) vort_gmres(t,omega,v,A,B,C),tspan,omega0);
toc;
% Elapsed time is 9.603830 seconds, on my Linux Machine
% Elapsed time is  seconds, on my MacBook Air
%}


%M = diag(diag(A));  %possible preconditioner
%{
% 6th IS THE BICGSTAB METHOD WITH PRECONDITIONING
tic;
[t,omega6_sol] = ode45(@(t,omega) vort_bstab_pcon(t,omega,v,A,B,C,M),tspan,omega0);
toc;
% Need to increase N >> 64 to notice difference in speed
%}

%{
% 7th IS THE GMRES METHOD WITH PRECONDITIONING
tic;
[t,omega5_sol] = ode45(@(t,omega) vort_gmres_pcon(t,omega,v,A,B,C,M),tspan,omega0);
toc;
% Need to increase N >> 64 to notice difference in speed
%}

%som1 = size(omega1_sol)      9x4096
%som2 = size(omega2_sol)      9x4096
%som3 = size(omega3_sol)      9x4096


%A1 = omega1_sol;
%A2 = omega2_sol;
%A3 = omega3_sol;
%A1b = omega4_sol;
%A2b = omega5_sol;
%save A1.dat A1 -ascii
%save A2.dat A2 -ascii
%save A3.dat A3 -ascii
%save A1b.dat A1b -ascii
%save A2b.dat A2b -ascii
%}


%-------------------------------------------------------------------------
% PLOTTING THE RESULTS AS A FUNCTION OF TIME AND SPACE:
%waterfall(X,t,omega1_sol)

%nFrames = 20;
%mov(1:nFrames) = struct('cdata',[], 'colormap',[]);

%writerObj = VideoWriter('movie1.avi')  % name of movie
%writerObj.FrameRate = 24;  % frames per second  (lasts about 6 sec for tspan[0:75]
%open(writerObj);
% PLOTTING MOVIE FRAMES

for j = 1:length(t)
    curw = reshape(omega3_sol(j,:),m,m);
    pcolor(X,Y,curw);
    shading interp;
    axis square;
    title({'Time evolution of Vorticity-Streamfunction'...
        'with Gaussian Bump Initial Condition'},'fontweight','bold','fontsize',14);
    drawnow;
    f = getframe(1);
    im = frame2im(f);
    [imind,cm] = rgb2ind(im,256);
    outfile = 'vortex.gif';
    
    if j==1
        imwrite(imind,cm,outfile,'gif','DelayTime',0,'loopcount',inf);
    else
        imwrite(imind,cm,outfile,'gif','DelayTime',0,'writemode','append');
    end
    pause(0.0001);
end

for j = 1:length(t)
    curw = reshape(omega3_sol(j,:),m,m);
    surf(X,Y,curw);
    shading interp;
    axis([-10 10 -10 10 -0.15 1.2]);
    title({'3D Time evolution of Vorticity-Streamfunction'...
        'with Gaussian Bump Initial Condition'},'fontweight','bold','fontsize',14);
    drawnow;
    f = getframe(1);
    im = frame2im(f);
    [imind,cm] = rgb2ind(im,256);
    outfile = 'vortex_cone.gif';
    
    if j==1
        imwrite(imind,cm,outfile,'gif','DelayTime',0,'loopcount',inf);
    else
        imwrite(imind,cm,outfile,'gif','DelayTime',0,'writemode','append');
    end
    pause(0.0001);
end




% The commented block below was used for creating plots used in a paper.
% The plots are a 3x3 array of the vorticity time evolution.
%{
plot8 = figure;
subplot(3,3,1)
curw = reshape(omega3_sol(1,:),m,m);
  
pcolor(X,Y,curw);
title(['t = ',num2str(t(1))])
colorbar;
shading interp;
colormap jet;
axis square;
    
for j=2:4
    subplot(3,3,j)
    curw = reshape(omega3_sol(2*j,:),m,m);
    pcolor(X,Y,curw);
    title(['t = ',num2str(round(t(2*j)))])
    colorbar;
    shading interp;
    colormap jet;
    axis square;
end

subplot(3,3,5)
curw = reshape(omega3_sol(2*7,:),m,m);
pcolor(X,Y,curw);
title(['t = ',num2str(round(t(2*7)))])
colorbar;
shading interp;
colormap jet;
axis square;

subplot(3,3,6)
curw = reshape(omega3_sol(2*10,:),m,m);
pcolor(X,Y,curw);
title(['t = ',num2str(round(t(2*10)))])
colorbar;
shading interp;
colormap jet;
axis square;

subplot(3,3,7)
curw = reshape(omega3_sol(2*12,:),m,m);
pcolor(X,Y,curw);
title(['t = ',num2str(round(t(2*12)))])
colorbar;
shading interp;
colormap jet;
axis square;

subplot(3,3,8)
curw = reshape(omega3_sol(2*14,:),m,m);
pcolor(X,Y,curw);
title(['t = ',num2str(round(t(2*14)))])
colorbar;
shading interp;
colormap jet;
axis square;

subplot(3,3,9)
curw = reshape(omega3_sol(2*16,:),m,m);
pcolor(X,Y,curw);
title(['t = ',num2str(round(t(2*16)))])
colorbar;
shading interp;
colormap jet;
axis square;

saveas(plot8, 'plot8.png', 'png');
%}