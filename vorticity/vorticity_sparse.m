function [A1, A2, A3] = vorticity_sparse()


m = 8;
n = m*m;
xspan = [-10:10];
[size1, size2] = size(xspan);
dx = (size2-1)/m;

e0 = zeros(n,1);    % vector of zeros
e1 = ones(n,1);     % vector of ones

e2 = e1;    % copy the one vector
e4 = e0;    % copy the zero vector

for j = 1:m
    e2(m*j) = 0;    % overwrite every m^th value with zero
    e4(m*j) = 1;    % overwrite every m^th value with one
end

e3(2:n,1) = e2(1:n-1,1); e3(1,1) = e2(n,1);  % shift to correct
e5(2:n,1) = e4(1:n-1,1); e5(1,1) = e4(n,1);  % positions

%place diagonal elements
A = spdiags([e1 e1 e5 e2 -4*e1 e3 e4 e1 e1],...
    [-(n-m) -m -m+1 -1 0 1 m-1 m (n-m)],...
    n,n);
A = (1/(dx^2))*A;
figure(10); spy(A); title('spy(A)'); saveas(figure(10), 'plot5.png', 'png');
A1 = full(A);


% Creating matrix B:
B = spdiags([e1 -e1 e1 -e1],...
    [-(n-m) -m m (n-m)],...
    n,n);
B = 1/(2*dx) * B;
figure(11); spy(B); title('spy(B)'); saveas(figure(11), 'plot6.png', 'png');
A2 = full(B);


% Creating matrix C:
C = spdiags([-e1 e1],...
    [-1 1],...
    m,m);
C(1,m) = -1; C(m,1) = 1;
C = blkdiag(C,C,C,C,C,C,C,C);
C = sparse(C);
C = 1/(2*dx) * C;
figure(12); spy(C); title('spy(C)'); saveas(figure(12), 'plot7.png', 'png');
A3 = full(C)
