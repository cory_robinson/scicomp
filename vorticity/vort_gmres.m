function rhs = vort_gmres( t,omega,v,A,B,C )
% bicgstab method for solving the vorticity equation

% [x,flag,relres,iter]=bicgstab(A,b,tol,maxit,M1,M2,x0);
[m,n] = size(A);
tol = 10^(-3);

psi = gmres(A,omega,[],tol,300,[],[],[]);

rhs = -(B*psi).*(C*omega) + (C*psi).*(B*omega) + v*A*omega;

end