function rhs = vort_Ab( t,omega,v,A,B,C )
% A\b method for solving the vorticity equation

psi = A\omega;
%sB = size(B)
%sBp1 = size(B*psi1)
%sC = size(C)
%sCo = size(C*omega)
rhs = -(B*psi).*(C*omega) + (C*psi).*(B*omega) + v*A*omega;

end

