function rhs = vort_LU( t,omega,v,A,B,C,L,U )
% LU method for solving the vorticity equation

x = L\omega;
psi = U\x;     % solve with LU NOTE: we could solve for psi with a 
                % one-liner:  psi = U\(L\omega)
%psi = U\(L\omega);
rhs = -(B*psi) .* (C*omega) + (C*psi) .* (B*omega) + v.*A*omega;

end