clear all; close all

% script for building the sparse matricies for the vorticity stream
% function in homework 3 problem 1
%
% Created by Cory R Robinson on 11/5/2013
%

% Create the matrix A:
n = 8;
x = [-10:10];
y = [-10:10];
e1 = ones(n^2,1);
A = spdiags([e1 -4*e1 e1],[-1 0 1],n,n);
A(1,n) = 1;  A(n,1) = 1;  % periodic boundary conditions
full(A)


% Create the matrix B:
%B = spdiags([-1*e1 0 e1],[