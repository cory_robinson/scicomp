function rhs = vort_gmres_pcon( t,omega,v,A,B,C,M )
% bicgstab method for solving the vorticity equation

% [x,flag,relres,iter]=bicgstab(A,b,tol,maxit,M1,M2,x0);
[m,n] = size(A);
tol = 10^(-3);

psi = gmres(A,omega,[],tol,300,M,[],[]);

rhs = -(B*psi).*(C*omega) + (C*psi).*(B*omega) + v*A*omega;

end