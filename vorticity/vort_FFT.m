function rhs = vort_FFT( t,omega,v,A,B,C,K )
% FFT method for solving the vorticity equation

[m,n] = size(K);    % 4096x4096    
omega_hat = fft2(reshape(omega,m,n));
psi_hat = omega_hat./K;
psi = reshape(real(ifft2(psi_hat)),size(omega));    % 4096x1

rhs = -(B*psi).*(C*omega) + (C*psi).*(B*omega) + v*A*omega;

end

