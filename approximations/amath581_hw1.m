clear all; close all

% ~/Desktop/UW/AMATH/AMATH581/Homework/Homework1/amath581_hw1.m
%
% This script is for completing Homework 1 problems for AMATH 581
%
% Created by Cory R Robinson on 10/3/2013
%

% Problem 1
% Building a Matrix
A = [34 45; 17 6];  % defined a 2x2 matrix A
save A1.dat A -ascii


% Problem 2
% Matrix Operations
% First we define 4 matrices and 3 vectors:
A = [1 2; -1 1];
B = [2 0; 0 2];
C = [2 0 -3; 0 0 -1];
D = [1 2; 2 3; -1 0];
x = [1; 0];
y = [0; 1];
z = [1; 2; -1];
% now perform 19 different matrix operations:
a = A + B;
b = 3*x - 4*y;
c = A*x;
d = B*(x-y);
e = D*x;
fx = D*y + z;
g = A*B;
h = B*C;
i = C*D;
save A2.dat a -ascii;
save A3.dat b -ascii;
save A4.dat c -ascii;
save A5.dat d -ascii;
save A6.dat e -ascii;
save A7.dat fx -ascii;
save A8.dat g -ascii;
save A9.dat h -ascii;
save A10.dat i -ascii;
% TODO: figure out how to save a sequence of files


% Problem 3
% RootFinding (Newton)
%
% Define functions and plot the curve
%x = -6:0.1:6;
%fx = -x - cos(x);    % function f(x)
%fxprime = -1 + sin(x);   % derivative of f(x)
%plot(x,fx), grid on;

[xvals_NR, iters_NR] = newtonraphson(-3, 10^-6, 100);
[midvals_BIS, iters_BIS] = bisection(-3, 1, 10^-6, 100);

A11 = xvals_NR;
A12 = midvals_BIS.';
A13 = [iters_NR iters_BIS];

save A11.dat A11 -ascii
save A12.dat A12 -ascii
save A13.dat A13 -ascii