function [xm, iters] = bisection(a, b, tol, maxiters)
% bisection(a,b,tol), a and b are intervval endpoints,
%   tol is the error tolerance
%   maxiters is the number of iterations to run through
%   bisection method for root finding

xl = a;     % initial left boundary
xr = b;     % initial right boundary

for n = 1:maxiters
    xm(n) = (xl+xr)/2;     % midpoint of interval
    fm = f(xm(n));         % function evaluated at the midpoint
    
    if fm > 0
        xl = xm(n);    % moves the interval to the right
    else
        xr = xm(n);    % moves the interval to the left
    end
    
    if abs(fm) < tol
        break   % tolerance has been reached, break the loop and finish
    end
    
end

if n == maxiters
    if abs(fm) > tol
        sprintf('WARNING:  has not yet converged after %d iterations', maxiters)
    end
end

iters = n;  % number of iterations until tol has been reached

end

