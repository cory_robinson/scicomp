function [xvals, iters] = newtonraphson(x1, tol, maxiters)
% newtonraphson(x1, tol), x1 is starting value of x, tol is error
% tolerance
%   newton-raphson root finding

%
x(1) = x1;  % initial value guess
%tol = 10^-6;    % tolerance of convergence
%maxiters = 100;
for n = 1:maxiters
    x(n+1) = x(n) - f(x(n))/f_prime(x(n));   % newton root formula
    xvals = x.';
    fxn1 = f(x(n+1));  % function fx at x(n+1)
    if abs(fxn1) < tol
        break
    end
    
end
if n == maxiters
    if abs(fxn1) > tol
        sprintf('WARNING:  has not yet converged after %d iterations', maxiters)
    end

end

iters = n;


end

