function y_prime = f_prime(x)
% y_prime = f_prime(x)
%   Derivative of function f(x): f'(x)

y_prime = -1 + sin(x);   % derivative of f(x)

end

