function [A4,A5] = fourier_decrypt(Fmat,permvec)




F = Fmat;
p0 = permvec;
r0 = size(p0);
q0 = 1:16;
sq = size(q0);
p = reshape(p0,4,4);       % reshape p vector to a 4x4 matrix
q = reshape(q0,4,4);
[H W D] =size(F);
r = size(p);
s = size(q);

figure(1);imshow(F);        % encrypted centered matrix

% WE ORIGINALLY GET THE fft(Fmat) WHICH HAS ALREADY BEEN SHIFTED.  SO THE
% FIRST THING TO DO IS TO EXTRACT THE BLOCKS FROM Fmat THEN PERMUTE THEM
% INTO THE CORRECT PLACES.
C = mat2cell(Fmat,[160 20 20 20 20 160],[160 20 20 20 20 160]);
D = C(2:5,2:5);     % these are the center blocks of the Fourier matrix
sd = size(D);
%sc = size(C);
[i,j] = ind2sub(r0,p0);    % indices of permvec
[m,n] = ind2sub(s,q);    % indices of unscrambled vec that i called q
%[u,v] = ind2sub(sd,D(:))

for i = 1:16
    E(i) = D(p(i));
end
E = reshape(E,4,4);



%{ 
%JUST CHECKING THAT THESE CELLS HAVE DIFFERENT VALUES i.e. THE CELLS
% ACTUALLY CHANGED WITH THE PERMUTATION VECTOR
E1 = E{1}
% 2.853063447609940 - 0.897071635387967i -1.569668019925372 + 1.420258362491744i
D1 = D{1}
% 0.011856696618663 + 0.011594475768331i  0.017132088288629 - 0.047663821571982i
E2 = E{2}
%  0.101387236929216 + 0.134764903722329i -0.013762486119083 + 0.186160676956321i
E3 = E{3}
% 2.243922467909663 + 1.212945448621310i  1.854404946218710 + 2.509050424321916i
%}



C(2:5,2:5) = E;     % this puts the decrypted center blocks back into
                    % the 400x400 matrix C
C2 = cell2mat(C);  % converts cell matrix back to a numerical matrix
figure(2); imshow(C2);   % decrypted centered matrix (center blocks should
                          % look different than in figure 1)
figure(3);imshow(abs(C2),[0,10^5]);
A4 = abs(C2);
%save A4.dat A4 -ascii

% AFTER PERMUTING THE BLOCKS TO THEIR CORRECT PLACES, WE CAN DO THE ifft
% STUFF AND SHOW IMAGES TO GET THE FINAL DECRYPTED RESULTS.
                          
F1 = ifftshift(C2);
figure(4); imshow(abs(F1),[0,10^5]);
F1 = ifft2(F1);
A5 = abs(F1);
%I(:,:,j) = imadjust(uint8(abs(F1)));


F = imadjust(uint8(abs(F1)));

%save A5.dat A5 -ascii


figure(5); imshow(F);
