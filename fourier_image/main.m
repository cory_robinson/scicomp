clear all; close all


[A1, A2, A3] = vorticity_sparse();
save A1.dat A1 -ascii
save A2.dat A2 -ascii
save A3.dat A3 -ascii

%{
load Fmat.mat;
load permvec.mat;
F = Fmat; p = permvec;
[A4,A5] = fourier_decrypt(F,p);
save A4.dat A4 -ascii
save A5.dat A5 -ascii
%}