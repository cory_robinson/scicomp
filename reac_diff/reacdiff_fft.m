function rhs = reacdiff_fft(t,uvec,n,beta,K,d1,d2)
% rhs function


% extract u and v vectors
% NOTE u0 and v0 are in fourier space
u0 = uvec(1:n^2);
v0 = uvec(n^2 + 1:2*n^2);

% need to reshape u0 and v0 as matrices so that we can define A, lambda, 
% and omega.  Also we need to get out of fourier space to compute A,
% lambda, and omega, so ifft2 the u0 and v0 matrices.
U = ifft2(reshape(u0,n,n));
V = ifft2(reshape(v0,n,n));

% define A, lambda, and omega; NOTE that we only need A^2, so A2 = A^2
% REGULAR SPACE
A2 = U.^2 + V.^2;
lambda = 1.-A2;       % need to subtract 1 elementwise
omega = -beta*A2;     % beta is passed in from the main program

% fft2(lambda_A.*U - omega_A.7*V)

% multiply U and V by K, and take their fft2; reshape as vectors
UK = reshape(fft2(U).*K, n^2, 1);
VK = reshape(fft2(V).*K, n^2, 1);

% Now that we are back in Fourier space, we account for the Laplacian term
% where (iK)^2 = -K; therefore we subtract the diffusion terms, D1(Lap^2)U
% and D2(Lap^2)V`

% In Fourier space we rrewrite the reaction diffusion system in terms of
% lambda, omega, UK, and VK.  So we must take the fft2 of the reaction part
% and the reshape the reaction in to a vector.  The result is the rhs
% vectors Utf and Vtf in Fourier Space
Utf = reshape(fft2(lambda.*U - omega.*V), n^2, 1) - d1.*UK;
Vtf = reshape(fft2(omega.*U + lambda.*V), n^2, 1) - d2.*VK;

rhs = [Utf; Vtf];    % rhs vectors are stacked u on top of v

end