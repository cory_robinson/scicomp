clear all; close all;

% main script for Homework 5
% Solving Reaction-Diffusion equation with spectral FFT and with Chebychev
% polynomials
%
% Created by Cory R Robinson on 11/26/2013
%




%--------------------------------------------------------------------------
% SPECTRAL FFT METHOD -----------------------------------------------------
%--------------------------------------------------------------------------

% Set up domain and parameters
n = 64;
m = 2;    % number of spirals
tspan = [0:0.5:27];
L = 20;   % length of xspan
x2 = linspace(-L/2,L/2,n+1);
x = x2(1:n);
y = x;
[X,Y] = meshgrid(x,y);
beta = 1;
d1 = 0.1;
d2 = d1;

kx=(2*pi/L)*[0:(n/2-1) (-n/2):-1];
kx(1)=10^-6;
ky=kx;
[KX,KY] = meshgrid(kx,ky);
K = KX.^2 + KY.^2;    % size 64x64

% Initial Conditions
u = tanh(sqrt(X.^2 + Y.^2)) .* cos(m*angle(X + 1i*Y) - (sqrt(X.^2 + Y.^2)));
v = tanh(sqrt(X.^2 + Y.^2)) .* sin(m*angle(X + 1i*Y) - (sqrt(X.^2 + Y.^2)));

% FFT u and v
ut = fft2(u);
vt = fft2(v);

% reshape u and v into column vectors
ut0 = reshape(ut,n^2,1);
vt0 = reshape(vt,n^2,1);

% stack the column vectors into the initial condition
IC_fft = [ut0; vt0];


% run ode solver
tic;
[t,usol_fft] = ode45(@(t,U) reacdiff_fft(t,U,n,beta,K,d1,d2),tspan,IC_fft);
toc;
% Elapsed time is 0.450367 seconds.

% Note that u_sol is in the fourier domain
A1 = real(usol_fft);
A2 = imag(usol_fft);
save A1.dat A1 -ascii
save A2.dat A2 -ascii



%{
plot9 = figure(1);
for j=1:length(t)
    ures_f = reshape(usol_fft(j,1:n^2),n,n);
    vres_f = reshape(usol_fft(j,(n^2)+1:2*(n^2)),n,n);
    uvres_f = real(ifft2(ures_f + vres_f));
    surf(X,Y,uvres_f);
    shading interp;
    drawnow;
    
    pause(0.1);
end
title('Spiral Waves using FFT Method (t=[0:0.5:27])');
xlabel('x');
ylabel('y');
%saveas(plot9, 'plot9.png', 'png');
%}



% Create gif images:
for j = 1:length(t)
    ures_f = reshape(usol_fft(j,1:n^2),n,n);
    vres_f = reshape(usol_fft(j,(n^2)+1:2*(n^2)),n,n);
    uvres_f = real(ifft2(ures_f + vres_f));
    pcolor(X,Y,uvres_f);
    shading interp;
    title('2-Arm Spiral Wave Solution to Reaction-Diffusion System','fontweight','bold','fontsize',14);
    drawnow;
    f = getframe(1);
    
    im = frame2im(f);
    [imind,cm] = rgb2ind(im,256);
    outfile = 'spiralwaves.gif';
    
    if j==1
        imwrite(imind,cm,outfile,'gif','DelayTime',0,'loopcount',inf);
    else
        imwrite(imind,cm,outfile,'gif','DelayTime',0,'writemode','append');
    end
    pause(0.0001);
end
%title({'2-Arm Spiral Wave Solution to Reaction-Diffusion System'});

for j = 1:length(t)
    ures_f = reshape(usol_fft(j,1:n^2),n,n);
    vres_f = reshape(usol_fft(j,(n^2)+1:2*(n^2)),n,n);
    uvres_f = real(ifft2(ures_f + vres_f));
    surf(X,Y,uvres_f);
    shading interp;
    title('2-Arm (3D) Spiral Wave Solution to Reaction-Diffusion System','fontweight','bold','fontsize',14);
    drawnow;
    f = getframe(1);
    %title({'2-Arm (3D) Spiral Wave Solution to Reaction-Diffusion System'});
    im = frame2im(f);
    [imind,cm] = rgb2ind(im,256);
    outfile = 'spiralwaves_3d.gif';
    
    if j==1
        imwrite(imind,cm,outfile,'gif','DelayTime',0,'loopcount',inf);
    else
        imwrite(imind,cm,outfile,'gif','DelayTime',0,'writemode','append');
    end
    pause(0.0001);
end
%title({'2-Arm (3D) Spiral Wave Solution to Reaction-Diffusion System'});


%{
plot11 = figure;
subplot(3,3,1)
ures_f = reshape(usol_fft(1,1:n^2),n,n);
vres_f = reshape(usol_fft(1,(n^2)+1:2*(n^2)),n,n);
uvres_f = real(ifft2(ures_f + vres_f));
pcolor(X,Y,uvres_f);
  
title(['t = ',num2str(t(1))])
colorbar;
shading interp;
colormap jet;
axis square;
    
for j=2:9
    subplot(3,3,j)
    ures_f = reshape(usol_fft(2*3*j,1:n^2),n,n);
    vres_f = reshape(usol_fft(2*3*j,(n^2)+1:2*(n^2)),n,n);
    uvres_f = real(ifft2(ures_f + vres_f));
    pcolor(X,Y,uvres_f);
    title(['t = ',num2str(round(t(2*3*j)))])
    colorbar;
    shading interp;
    colormap jet;
    axis square;
end
saveas(plot11, 'plot11.png', 'png');
%}



%{
%--------------------------------------------------------------------------
% CHEBYCHEV POLYNOMIAL METHOD ---------------------------------------------
%--------------------------------------------------------------------------

% n=31 leaves us with the 9x1922 solution vector that we need
n = 31;
m = 1;    % number of spirals
tspan = [0:0.5:4];
beta = 1;
d1 = 0.1;
d2 = d1;

[D,x] = cheb(n-1);
[X,Y] = meshgrid(10*x,10*x);
D = D/10;

% build the differentiation matrix with Dirichlet BC
D2 = D^2;
D2(1,:) = zeros(1,n);
D2(n,:) = zeros(1,n);

% Laplacian matrix
I = eye(length(D2));
Lap = sparse(kron(D2,I) + kron(I,D2));


% Initial Conditions defined on the Chebychev grid
u = tanh(sqrt(X.^2 + Y.^2)) .* cos(m*angle(X + 1i*Y) - (sqrt(X.^2 + Y.^2)));
v = tanh(sqrt(X.^2 + Y.^2)) .* sin(m*angle(X + 1i*Y) - (sqrt(X.^2 + Y.^2)));

% reshape u and v into column vectors
u0 = reshape(u,n^2,1);
v0 = reshape(v,n^2,1);
IC_cheb = [u0; v0];



% run the ode solver
tic;
[t, usol_cheb] = ode45(@(t,uvec) reacdiff_cheb(t,uvec,n,Lap,beta,d1,d2,m),...
    tspan,IC_cheb);
toc;
% Elapsed time is 0.504158 seconds.

A3 = usol_cheb;
save A3.dat A3 -ascii


% PLOTTING MOVIE FRAMES
plot10 = figure;
for j=1:length(t)
    ures_c = reshape(usol_cheb(j,1:n^2),n,n);
    vres_c = reshape(usol_cheb(j,(n^2)+1:2*(n^2)),n,n);
    uvres_c = real(ures_c + vres_c);
    pcolor(X,Y,uvres_c);
    shading interp;
    drawnow;
    
    pause(0.1);
end
title('Spiral Waves using Chebychev Method (t=4)');
xlabel('x');
ylabel('y');
saveas(plot10, 'plot10.png', 'png');
%}