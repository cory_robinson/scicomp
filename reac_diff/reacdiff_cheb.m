function rhs = reacdiff_cheb(t,uvec,n,Lap,beta,d1,d2,m)
% rhs function


% extract u and v vectors; reshape as matrices (I think this may also 
% work w/o reshaping as matrices. i.e. just work with vectors throughout
% entire rhs function? Takes about 1 second longer for ode45 to run 
% with U and V as matrices)
U = uvec(1:n^2);  % 961x1(n=31)    900x1(n=30)
V = uvec(n^2 + 1:2*n^2);   % 900x1

% define A, lambda, and omega, NOTE A2 = A^2
% These are Vectors:
A2 = U.^2 + V.^2;    
lambda = 1.-A2;      
omega = -beta*A2;    % beta is passed in from the main program

% Rewrite everything in the reaction diffusion system, i.e. in terms of
% lambda and omega with the reaction part reshaped as a vector.  The
% diffusion part will the Lap matrix times a vector, hence a vector.
% The result is two vectors Ut and Vt obtained by using Chebychev polynomials
Utc = lambda.*U - omega.*V + d1.*Lap*U;
Vtc = omega.*U + lambda.*V + d2.*Lap*V;

rhs = [Utc; Vtc];

end