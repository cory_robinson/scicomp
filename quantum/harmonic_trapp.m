function psi = harmonic_trapp( x,y,epsilon )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

psi1 = y(2);
psi2 = (x^2 - epsilon)*y(1);
psi = [psi1; psi2];

end