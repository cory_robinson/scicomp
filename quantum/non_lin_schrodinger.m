function psi = non_lin_schrodinger( x,y,gamma,epsilon )
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

K = 1;
psi1 = y(2);
psi2 = (gamma*abs(y(1))^2 + K*x^2 - epsilon)*y(1);
psi = [psi1; psi2];


end

