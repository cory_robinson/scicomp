function [ eigenfunctions_neg, energy_neg, eigenfunctions_pos, energy_pos ]...
    = non_lin_shoot( non_lin_schrodinger )



tol = 10^(-4);
L = 2;
xspan = [-L:0.1:L];
K = 1;
A_start = 0.2;
gamma1 = 0.05;
col = ['r', 'b'];

epsilon_start = .1;
for modes = 1:2
    epsilon = epsilon_start;
    depsilon = 0.1;
    A = A_start;
    for j = 1:1000
        for k = 1:1000
            % DEFINE IC
            x0 = [A A*sqrt(L^2 - epsilon)];
            [x,y] = ode45 (@(x,y) non_lin_schrodinger(x,y,gamma1,epsilon),xspan,x0);
            % NORMALIZE
            norm = trapz(x,y(:,1).*y(:,1));   % normalizing the eigenfunctions
            
            if abs(sqrt(norm) - 1) < tol;
                break
            else
                A = A/sqrt(norm);
            end
        end
        
        % now check if BC at right boundary is satisfied within tolerance.
        % If not then do the same epsilon shooting as in the linear
        % shooting method:
        if abs(y(end,2) + sqrt(L^2 - epsilon)*y(end,1)) < tol  % check for convergence
            break       % epsilon is fine break out of convergence loop
        end
        if (-1)^(modes+1)*(y(end,2) + sqrt(L^2 - epsilon)*y(end,1)) > 0
            epsilon = epsilon + depsilon;
        else
            epsilon = epsilon - depsilon/2;
            depsilon = depsilon/2;
        end
        
                  
    end
    epsilon_start = epsilon + 0.1;
    energy_neg(modes) = epsilon;
    E = y(:,1)/sqrt(norm);
    eigenfunctions_neg(:,modes) = abs(E); % put the eigenfunctions into a matrix
    plot3 = plot(x, E, col(modes)); hold on     % plot modes
    xlabel('x');
    ylabel('\psi _n(x)');
    % implementing a multiline title with a cell array {}
    title('Plot of 1st Two Eigenmodes of non-linear QHO using the Shooting Method with \gamma = 0.05');
    legend('mode 1','mode 2',3)
end
figure(3);
saveas(plot3, 'plot3.png', 'png'); hold off
eigenfunctions_neg(:,:);
energy_neg;

%-----------------------------------
% positive gamma
col = ['g', 'm'];
A_start = 0.2;
gamma2 = -0.05;
epsilon_start = .01;
for modes = 1:2
    epsilon = epsilon_start;
    depsilon = 0.05;
    A = A_start;
    for j = 1:1000
        for k = 1:1000
            % DEFINE IC
            x0 = [A A*sqrt(L^2 - epsilon)];
            [x,y] = ode45 (@(x,y) non_lin_schrodinger(x,y,gamma2,epsilon),xspan,x0);
            % NORMALIZE
            norm = trapz(x,y(:,1).*y(:,1));   % normalizing the eigenfunctions
            
            if abs(sqrt(norm) - 1) < tol;
                break
            else
                A = A/sqrt(norm);
            end
        end
        
        % now check if BC at right boundary is satisfied within tolerance.
        % If not then do the same epsilon shooting as in the linear
        % shooting method:
        if abs(y(end,2) + sqrt(L^2 - epsilon)*y(end,1)) < tol  % check for convergence
            break       % epsilon is fine break out of convergence loop
        end
        if (-1)^(modes+1)*(y(end,2) + sqrt(L^2 - epsilon)*y(end,1)) > 0
            epsilon = epsilon + depsilon;
        else
            epsilon = epsilon - depsilon/2;
            depsilon = depsilon/2;
        end
        
                  
    end
    epsilon_start = epsilon + 0.1;
    energy_pos(modes) = epsilon;
    E = y(:,1)/sqrt(norm);
    eigenfunctions_pos(:,modes) = abs(E); % put the eigenfunctions into a matrix

    plot4 = plot(x, E, col(modes)); hold on     % plot modes
    xlabel('x');
    ylabel('\psi _n(x)');
    % implementing a multiline title with a cell array {}
    title('Plot of 1st Two Eigenmodes of non-linear QHO using the Shooting Method with \gamma = -0.05');
    legend('mode 1','mode 2',3)
end
figure(4);
saveas(plot4, 'plot4.png', 'png');
eigenfunctions_pos(:,:);
energy_pos;

end

