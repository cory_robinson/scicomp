clear all; close all

% main script for AMATH 581 HW 2
%
% Problem 1:  Linear shooting method for finding energy states of a quantum
% harmonic oscillator.

% Problem 2:  Direct Method for finding energy states of the quantum
% harmonic oscillator.

% Problem 3:  Nonlinear shooting method for finding energy states of a
% nonlinear quantum harmonic oscllator
%
% Created by Cory R Robinson 
% Last update 10/31/2013
%


% Problem 1:
[eigenfunctions, energy] = lin_shoot(@harmonic_trapp);
A1 = eigenfunctions(:,1);
A2 = eigenfunctions(:,2);
A3 = eigenfunctions(:,3);
A4 = eigenfunctions(:,4);
A5 = eigenfunctions(:,5);
A6 = energy.'
save A1.dat A1 -ascii
save A2.dat A2 -ascii
save A3.dat A3 -ascii
save A4.dat A4 -ascii
save A5.dat A5 -ascii
save A6.dat A6 -ascii



% Problem 2:
[eigenfunctions2, energy2] = direct_method();
A7 = eigenfunctions2(:,1);
A8 = eigenfunctions2(:,2);
A9 = eigenfunctions2(:,3);
A10 = eigenfunctions2(:,4);
A11 = eigenfunctions2(:,5);
A12 = energy2
save A7.dat A7 -ascii
save A8.dat A8 -ascii
save A9.dat A9 -ascii
save A10.dat A10 -ascii
save A11.dat A11 -ascii
save A12.dat A12 -ascii


% Problem 3:
[eigenfunctions3, energy3, eigenfunctions4, energy4]...
    = non_lin_shoot(@non_lin_schrodinger);
A13 = eigenfunctions4(:,1);
A14 = eigenfunctions4(:,2);
A15 = energy4.'
A16 = eigenfunctions3(:,1);
A17 = eigenfunctions3(:,2);
A18 = energy3.'
save A13.dat A13 -ascii
save A14.dat A14 -ascii
save A15.dat A15 -ascii
save A16.dat A16 -ascii
save A17.dat A17 -ascii
save A18.dat A18 -ascii
