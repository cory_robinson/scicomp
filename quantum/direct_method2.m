clear all; close all;
%function [ eigenfunctions, energy_small ] = direct_method2()

% function for deirect method of Problem 2:
%

L = 4;
dx = 0.1;
xspan = [-L:dx:L];
N = 79;
K = 1;

% building tridiagonal matrix A:
e1 = ones(N,1);
A = diag(-e1,-1) + diag(-e1,1);
size(A);

for i = 1:N-1
    % center difference for the matrix A_(79x79) this gives the diagonal of 
    % (rows x columns) = (2-78 x 2-78)
    A(i+1,i+1) = 2 + (dx^2)*K*xspan(i)^2;
end

A(1,1) = 2/3 + (dx^2)*K*xspan(1)^2;
A(1,2) = -2/3;   % position A(1,2) - upper diagonal
A(N+1,N) = -2/3; % position A(80,79) - lower diagonal  (N=79)
A(N+1,N+1) = 2/3 + (dx^2)*K*xspan(N-1)^2;  % position(80,80)

A = A./(dx^2);
size(A);
%format short
%[phi, epsilon] = eig(A);
[phi_small, epsilon_small] = eigs(A,5,'sm');
energy_small = sort(diag(epsilon_small))


% add boundary conditions onto the eigenvectors:
M = 5;
for i = 1:M
    phi_left(i) = (4*phi_small(1,i) - phi_small(2,i))/(3+2*dx*sqrt(L^2 - energy_small(i)));
    phi_right(i) = (4*phi_small(M-1,i) - phi_small(M-2,i))/(3+2*dx*sqrt(L^2 - energy_small(i)));
end

phi_small = [phi_left; phi_small; phi_right];
size(phi_small)

col = ['r', 'b', 'g', 'c', 'm'];    % eigenfunction colors
%{
norm = trapz(xspan,phi_small(:,i).*phi_small(:,i));   % normalize the eigenfunctions
E(:,1) = -phi_small(:,1)/sqrt(norm);
E(:,2) = phi_small(:,2)/sqrt(norm);
E(:,3) = -phi_small(:,3)/sqrt(norm);
E(:,4) = phi_small(:,4)/sqrt(norm);
E(:,5) = -phi_small(:,5)/sqrt(norm);
%plot(xspan, E(:,:)); hold on     % plot modes
%}



%eigenfunctions(:,1:5) = abs(E);     % put the eigenfunctions into a matrix
%energy_small = epsilon_small(:,1);  % vector of 5 smallest eigenvalues
%plot(xspan, eigenfunctions); hold on     % plot modes
%end