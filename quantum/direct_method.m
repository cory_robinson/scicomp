function [ eigenfunctions, energy_small ] = direct_method()

% function for deirect method of Problem 2:
%

L = 4;
dx = 0.1;
xspan = [-L:dx:L];
N = 79;
K = 1;

% building tridiagonal matrix A:
A = zeros(N,N);
for i = 1:N-1
    % center difference for the matrix A_(79x79) this gives the diagonal of 
    % (rows x columns) = (2-78 x 2-78)
    A(i+1,i+1) = 2 + (dx^2)*K*xspan(i+2)^2;
end

for i = 1:N-2
    % this gives the upper diagonal of (rows x columns) = (2-78 x 3-79)
    A(i+1,i) = -1;
end
A(1,1) = 2/3 + (dx^2)*K*xspan(2)^2;
A(1,2) = -2/3;   % position A(1,2) - upper diagonal

for i = 1:N-2
    % this gives the lower diagonal of (rows x columns) = (2-78 x 1-77)
    A(i+1,i+2) = -1;
end
A(N,N-1) = -2/3; % position A(79,78) - lower diagonal
A(N,N) = 2/3 + (dx^2)*K*xspan(N+1)^2;

A = A./(dx^2);
%format short
%[phi, epsilon] = eig(A);
[phi_small, epsilon_small] = eigs(A,5,'sm');
energy_small = diag(epsilon_small);   % these are my eigenvalues

% phi_small are eigenvectors corresponding to eigenvalues

% add the boundary conditions to the eigenvectors

M = 5;
for i = 1:M
    denominator = (3+2*dx*sqrt(L^2 - energy_small(i)));
    phi_left(i) = (4*phi_small(1,i) - phi_small(2,i))/denominator;
    phi_right(i) = (4*phi_small(M-1,i) - phi_small(M-2,i))/denominator;
end

phi_small = [phi_left; phi_small; phi_right];
%size(phi_small)

col = ['r', 'b', 'g', 'c', 'm'];    % eigenfunction colors


% Eigenfunctions
norm = trapz(xspan,phi_small(:,i).*phi_small(:,i));   % normalize the eigenfunctions
E(:,1) = abs(phi_small(:,1)/sqrt(norm));
E(:,2) = abs(phi_small(:,2)/sqrt(norm));
E(:,3) = abs(phi_small(:,3)/sqrt(norm));
E(:,4) = abs(phi_small(:,4)/sqrt(norm));
E(:,5) = abs(phi_small(:,5)/sqrt(norm));
%plot(xspan, E(:,:)); hold on     % plot modes



energy_small = sort(energy_small);
eigenfunctions(:,1:5) = E;     % put the eigenfunctions into a matrix
%energy_small = epsilon_small(:,1);  % vector of 5 smallest eigenvalues
plot(xspan, eigenfunctions); hold on     % plot modes
end

