function [ eigenfunctions, energy ] = lin_shoot( harmonic_trapp )


% linear shooting function for AMATH 581 HW 2
% QUANTUM HARMONIC OSCILLATOR/BOUNDARY VALUE PROBLEMS
%
% Created by Cory R Robinson
%

% Problem 1
% Finding the first five normalized eigenfunctions and eigenvalues of the
% probability density evolution in a one-dimensional harmonic trapping
% potential.
%
% setting parameters
tol = 10^(-4);      % tolerance level
col = ['r', 'b', 'g', 'c', 'm'];    % eigenfunction colors
n0 = 1;      % define the parameter n0 as a guess for first eigenvalue
A = 1;      % define the initial slope at x = -1
L = 4;
x0 = [A A*sqrt(L^2 - n0)];     % initial conditions y(-L) = 1, y1'(-L) = A
xspan = [-L:0.1:L];


% start shooting method
epsilon_start = n0;    % beginning value for eigenvalue epsilon
for modes = 1:5     % begin mode loop
     
    epsilon = epsilon_start;  % initial value of eigenvalue epsilon
    depsilon = n0/2;     % default step size in epsilon
    for j = 1:1000      % convergence loop for epsilon
        [x, y] = ode45( @(x,y) harmonic_trapp(x,y,epsilon),xspan,x0); 
        y_calc = -sqrt(L^2 - epsilon)*A;   %y(end,1);
        if abs(y(end,2) - y_calc) < tol     % check for convergence
            epsilon;        % write out eigenvalue
            %error = abs(y(end,2) - y_calc)
            break                  % get out of convergence loop
        end
            
        if abs(y(end,2) + sqrt(L^2 - epsilon)*y(end,1)) < tol  % check for convergence
            break       % epsilon is fine break out of convergence loop
        end
        if (-1)^(modes+1)*(y(end,2) + sqrt(L^2 - epsilon)*y(end,1)) > 0
            epsilon = epsilon + depsilon;
        else
            epsilon = epsilon - depsilon/2;
            depsilon = depsilon/2;
        end
        
    energy(modes) = epsilon;
    epsilon_start = epsilon + 0.1;    % after finding the eigenvalue, pick
                                      % a new starting value for next mode
    norm = trapz(x,y(:,1).*y(:,1));   % normalize the eigenfunctions
    E = y(:,1)/sqrt(norm);
    
    eigenfunctions(:,modes) = abs(E); % put the eigenfunctions into a matrix
    
end     % ends mode loop
format long
plot1 = plot(x, E,col(modes)); hold on     % plot modes
xlabel('x');
ylabel('\psi _n(x)');
% implementing a multiline title with a cell array {}
title('Plot of 1st Five Eigenmodes of QHO using the Shooting Method');
legend('mode 1','mode 2','mode 3','mode 4','mode 5',3)

%saveas(b_plot, '~/Desktop/UW/AMATH/AMATH584/Homework/Homework5/prob3.png', 'png');
eigenfunctions(:,:);
energy.';

end
figure(3);
saveas(plot1, 'plot1.png', 'png');
end