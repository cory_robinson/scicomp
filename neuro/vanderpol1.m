function dYdt = vanderpol1(t,Y)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

% initial conditions
y = Y(1);   % y(0) = sqrt(3)
x = Y(2);   % dy(t)/dt = 1
epsilon = 1;
dydt = x;
dxdt = -y - epsilon*(y^2 - 1)*x;
dYdt = [dydt; dxdt];

end


