clear all; close all
% Main script for running:
%   forwardEuler.m
%   heun.m
%   vanderpol01.m
%   vanderpol1.m
%   vanderpol20.m
%   fitzhugh.m
% for AMATH 581 Homework 1
%
% Created by Cory R Robinson on 10/15/2013


%------------------------------------------------------------------
% Problem 1 Part (a)
% Implementing Euler's Method by calling the forwardEuler.m function

dt = 2.^-[2:8];         % vector of time steps - delta_t

[y_e, error_e] = forwardEuler(dt);
y_e = y_e.';

% plotting the log of the errors against the log of the tolerance
% commented for grading purposes, uncomment to see graph:
%plot(log(dt), log(error_e))
%hold on
%xlabel('log(\Delta t)')
%ylabel('log(error)')
%title('Plot of error vs step size in the Forward Euler Method & Heun''s Method')
% Notice the error gets smaller as the time steps decrease; this is the
% correct relationship.

% p45, p23, p113 are the slopes of the plotted error data
%length(error)
%length(dt)
%log_dt = log(dt)
%err = error
%log_err = log(error)
p_e = polyfit(log(dt), log(error_e), 1);
A3 = p_e(1);   % slope of the error line for eulers method

save A1.dat y_e -ascii
save A2.dat error_e -ascii
save A3.dat A3 -ascii


%--------------------------------
% Problem 1 Part (b)
% Implementing Heun's Method by calling heun.m function

[y_h, error_h] = heun(dt);
y_h = y_h.';

% plotting error vs time step for heuns method
% On the same plot as that of Eulers method for comparison,
% commented out for grading purposes
%plot(log(dt), log(error_h),'r')
%legend('Euler''s Method', 'Heun''s Method',4)

p_h = polyfit(log(dt), log(error_h), 1);
A6 = p_h(1);    % slope of the error line for heuns method

save A4.dat y_h -ascii
save A5.dat error_h -ascii
save A6.dat A6 -ascii



%--------------------------------------------------------------------
% Problem 2(a)
% van der Pol oscillator
y0 = [sqrt(3); 1];
tspan = [0:0.5:32];
[T01,Y01] = ode45( @vanderpol01, tspan, y0);
[T1,Y1] = ode45( @vanderpol1, tspan, y0);
[T20,Y20] = ode45( @vanderpol20, tspan, y0);
A7 = [Y01(:,1) Y1(:,1) Y20(:,1)];
save A7.dat A7 -ascii


% Problem 2(b)
% error tolerance and average step sizes for van der Pol oscillator
y0 = [2; pi^2];
TOL = 10.^-[4:10];
tspan = [0,32];
for i = 1:7
    
    options = odeset('AbsTol',TOL(i),'RelTol',TOL(i));
    [T45, Y] = ode45(@vanderpol1, tspan, y0, options);
    [T23, Y] = ode23(@vanderpol1, tspan, y0, options);
    [T113, Y] = ode113(@vanderpol1, tspan, y0, options);
    avg_dt45(i) = mean(diff(T45));      % average step size for ode45
    avg_dt23(i) = mean(diff(T23));      % average step size for ode23
    avg_dt113(i) = mean(diff(T113));    % average step size for ode113
end

% plotting the log of the errors against the log of the tolerance
% commented for grading purposes, uncomment to see graph:
%plot(log(avg_dt45), log(TOL))
%hold on
%plot(log(avg_dt23), log(TOL),'r')
%plot(log(avg_dt113), log(TOL),'g')
%xlabel('log(\Delta t)')
%ylabel('log(TOL)')
%legend('ode45','ode23','ode113',2)

% p45, p23, p113 are the slopes of the plotted error data
p45 = polyfit(log(avg_dt45), log(TOL), 1);
p23 = polyfit(log(avg_dt23), log(TOL), 1);
p113 = polyfit(log(avg_dt113), log(TOL), 1);

% saving the slopes of the error data p45(1), p23(1), p113(1)
A8 = p45(1);
A9 = p23(1);
A10 = p113(1);
save A8.dat A8 -ascii
save A9.dat A9 -ascii
save A10.dat A10 -ascii



%-----------------------------------------------------------------
% Problem 3
% Interaction between neurons.  Implementation of two Fitzhugh neurons
% coupled via linear coupling:

% defining parameters
a1 = 0.05;
a2 = 0.25;
b = 0.01;
c = 0.01;
I = 0.1;
d12 = [0 0 -0.1 -0.3 -0.5];
d21 = [0 0.2 0.2 0.2 0.2];

% initial conditions
V = [0.1, 0.1];
W = [0, 0];
VW = [V W];


format long
%set error bounds for integration
%options=odeset('RelTol',10^(-10),'AbsTol',[10^(-10)*ones(1,4)]);

%for i = 1:5
%    t = 100;
%    dt = 0.5;
%    [t,dVW] = ode15s(@(t,var) fitzhugh(t,var,a1,a2,b,c,I,d12(i),d21(i)),...
%    [0:dt:t],VW);
%end

t = 100;
dt = 0.5;
[t,A11] = ode15s(@(t,var) fitzhugh(t,var,a1,a2,b,c,I,d12(1),d21(1)),...
    [0:dt:t],VW);

t = 100;
dt = 0.5;
[t,A12] = ode15s(@(t,var) fitzhugh(t,var,a1,a2,b,c,I,d12(2),d21(2)),...
    [0:dt:t],VW);

t = 100;
dt = 0.5;
[t,A13] = ode15s(@(t,var) fitzhugh(t,var,a1,a2,b,c,I,d12(3),d21(3)),...
    [0:dt:t],VW);

t = 100;
dt = 0.5;
[t,A14] = ode15s(@(t,var) fitzhugh(t,var,a1,a2,b,c,I,d12(4),d21(4)),...
    [0:dt:t],VW);

t = 100;
dt = 0.5;
[t,A15] = ode15s(@(t,var) fitzhugh(t,var,a1,a2,b,c,I,d12(5),d21(5)),...
    [0:dt:t],VW);


save A11.dat A11 -ascii
save A12.dat A12 -ascii
save A13.dat A13 -ascii
save A14.dat A14 -ascii
save A15.dat A15 -ascii

