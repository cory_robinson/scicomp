function dVWdt = fitzhugh(t,VW,a1,a2,b,c,I,d12,d21)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% initial values passed into these variables
v1 = VW(1);
v2 = VW(2);
w1 = VW(3);
w2 = VW(4);

%set up system of equations
dv1dt = -v1^3 + (1+a1)*v1^2 - a1*v1 - w1 + I + d12*v2;
dw1dt = b*v1 - c*w1;
dv2dt = -v2^3 + (1+a2)*v2^2 - a2*v2 - w2 + I + d21*v1;
dw2dt = b*v2 - c*w2;
dVWdt = [dv1dt; dv2dt; dw1dt; dw2dt];
