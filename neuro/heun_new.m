function [y, error] = heun(dt)
% This function implements the Improved Euler method (or Heun's Method) for approximating the solutiond to ODE's
% We can che the inline function, dydt, on line 9 to accomodate any rhs ODE
%
% Created by Cory r Robinson on 10/14/2013
%

y(1) = pi/sqrt(2);      % initial condition
dydt = @(t,y) -3*y*sin(t);   % inline function for RHS of ODE
for i = 1:length(dt)
    tspan = 0:dt(i):5;
    y_true = pi*exp(3*(cos(tspan) - 1))/sqrt(2);
    for n = 1:length(tspan)-1
        y(n+1) = y(n) + (dt(i)/2)*( dydt(tspan(n),y(n)) +...
            dydt(tspan(n)+dt(i),y(n)+dt(i)*dydt(tspan(n),y(n))) );  % heun iteration
    end
    error(i) = norm((y_true - y),2);   % error norm
end


end

